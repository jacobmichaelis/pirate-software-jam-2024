extends WorldEnvironment

@onready var _animator: AnimationPlayer = find_child("AnimationPlayer")

func village_lighting():
	_animator.play("enter_village")

func woods_lighting():
	_animator.play("exit_village")
