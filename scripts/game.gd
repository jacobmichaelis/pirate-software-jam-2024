extends Node3D

@export var _environment: WorldEnvironment
@export var _player: CharacterBody3D
@export var _spredding: CharacterBody3D

@onready var _spredding_spawn: Marker3D = find_child("SpreddingSpawn")

func game_over():
	_spredding.global_position = _player.spredding_kill_mark.global_position
	Global.game_over()

func _on_enter_village(body: Node3D):
	print("Entered village")
	_spredding.global_position = _spredding_spawn.global_position
	_spredding.speed = 0
	_player.turn_off_flashlight()
	_environment.village_lighting()
	Global.exit_woods_audio()

func _on_exit_village(body: Node3D):
	print("Exited village")
	_spredding.global_position = _spredding_spawn.global_position
	_spredding.speed = 2
	_player.turn_on_flashlight()
	_environment.woods_lighting()
	Global.enter_woods_audio()
