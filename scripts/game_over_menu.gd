extends CanvasLayer

func _on_restart_button_pressed():
	Global.restart_game()

func _on_quit_button_pressed():
	Global.quit_game()
