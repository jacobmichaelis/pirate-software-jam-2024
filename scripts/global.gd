extends Node

var _started: bool = false
var _paused: bool = false

func _ready():
	$StartMenu.show()
	get_tree().paused = true

func _input(event):
	if event.is_action_pressed("exit"):
		if not _started:
			get_tree().quit()
		elif _paused:
			resume_game()
		else:
			pause_game()

func start_game():
	_started = true
	$StartMenu.hide()
	get_tree().paused = false
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func pause_game():
	_paused = true
	$PauseMenu.show()
	get_tree().paused = true
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE

func resume_game():
	_paused = false
	$PauseMenu.hide()
	get_tree().paused = false
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func game_over():
	_started = false
	$GameOverMenu.show()
	get_tree().paused = true
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	stop_woods_audio()
	$DeathScreenAudio.play()

func restart_game():
	_started = true
	$GameOverMenu.hide()
	get_tree().reload_current_scene()
	get_tree().paused = false
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	$DeathScreenAudio.stop()

func quit_game():
	get_tree().quit()

func exit_woods_audio():
	if $IntoTheWoodsIntro.playing:
		var tween = get_tree().create_tween()
		tween.tween_property($IntoTheWoodsIntro, "volume_db", -40.0, 3)
		tween.tween_callback(stop_woods_audio)
	if $IntoTheWoodsLoop.playing:
		var tween = get_tree().create_tween()
		tween.tween_property($IntoTheWoodsLoop, "volume_db", -40.0, 3)
		tween.tween_callback(stop_woods_audio)

func stop_woods_audio():
	$IntoTheWoodsIntro.stop()
	$IntoTheWoodsLoop.stop()

func enter_woods_audio():
	$IntoTheWoodsIntro.play()
	var tween = create_tween()
	tween.tween_property($IntoTheWoodsIntro, "volume_db", 0, 3)

func start_woods_loop():
	$IntoTheWoodsLoop.play()
