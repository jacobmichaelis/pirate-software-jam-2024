extends MeshInstance3D

class_name Interactable

@onready var _highlight_shader: ShaderMaterial = mesh.surface_get_material(0).next_pass
@onready var _outline_shader: ShaderMaterial = _highlight_shader.next_pass
@onready var _interact_label: Label3D = find_child("Label3D")
@onready var _interaction_menu: CanvasLayer = find_child("InteractionMenu")

var targeted: bool = false:
	set(val):
		targeted = val
		if val:
			_highlight_shader.set_shader_parameter("strength", 0.5)
			_outline_shader.set_shader_parameter("outline_width", 2.0)
			_interact_label.show()
		else:
			_highlight_shader.set_shader_parameter("strength", 0.0)
			_outline_shader.set_shader_parameter("outline_width", 0.0)
			_interact_label.hide()

var interacting: bool = false:
	set(val):
		interacting = val
		if val:
			Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
			_interaction_menu.show()
		else:
			Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
			_interaction_menu.hide()

func _ready():
	_interact_label.hide()
	_interaction_menu.hide()

func _on_pick_up_button_pressed():
	queue_free()
	interacting = false

func _on_cancel_button_pressed():
	interacting = false
