extends CanvasLayer

func _on_resume_button_pressed():
	Global.resume_game()

func _on_quit_button_pressed():
	Global.quit_game()
