extends CharacterBody3D

signal found_spredding

# TODO: make this adjustable in settings
@export var SPEED: float = 5.0
@export var JUMP_VELOCITY: float = 4.5
@export var MOUSE_SENSITIVITY: float = 0.25

@onready var _camera: Camera3D = find_child("Camera3D")
@onready var _flashlight: SpotLight3D = find_child("Flashlight")
@onready var _interactor: RayCast3D = find_child("Interactor")
@onready var _line_of_sight: RayCast3D = find_child("LineOfSight")
@onready var _crosshair: ColorRect = find_child("Crosshair")
@onready var _fps: Label = find_child("FPS")

@onready var spredding_kill_mark: Marker3D = find_child("SpreddingKillMark")

const _TILT_LOWER_LIMIT := deg_to_rad(-90.0)
const _TILT_UPPER_LIMIT := deg_to_rad(90.0)
const _CROSSHAIR_COLOR = Color(0.5, 0.5, 0.5, 0.5)

var _mouse_input: bool = false
var _mouse_rotation: Vector3
var _rotation_input: float
var _tilt_input: float
var _player_rotation: Vector3
var _camera_rotation: Vector3

var _target_interactable: Interactable = null

func _input(event):
	if event.is_action_pressed("interact"):
		if _target_interactable != null:
			_target_interactable.interacting = !_target_interactable.interacting

func _unhandled_input(event):
	_mouse_input = event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED
	if _mouse_input:
		_rotation_input = -event.relative.x * MOUSE_SENSITIVITY
		_tilt_input = -event.relative.y * MOUSE_SENSITIVITY

func _process(delta):
	_fps.text = "FPS " + str(Engine.get_frames_per_second())
	# https://www.youtube.com/watch?v=Wi6isHhNVDU
	var interactable_collider = _interactor.get_collider()
	var line_of_sight_collider = _line_of_sight.get_collider()
	if line_of_sight_collider != null:
		print("Found Spredding? ", line_of_sight_collider)
		found_spredding.emit()
	
	if interactable_collider != null and interactable_collider is StaticBody3D:
		var interactable = interactable_collider.get_parent() as Interactable
		if interactable != _target_interactable:
			if interactable != null:
				interactable.targeted = true
				_crosshair.color = Color.WHITE
			if _target_interactable != null:
				_target_interactable.targeted = false
				_crosshair.color = _CROSSHAIR_COLOR
		_target_interactable = interactable
	else:
		if _target_interactable != null:
			_target_interactable.targeted = false
			_crosshair.color = _CROSSHAIR_COLOR
			_target_interactable = null

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta
		
	_update_camera(delta)
	
	# if in a menu, don't capture movement
	if _target_interactable != null and _target_interactable.interacting:
		return

	# Handle jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir = Input.get_vector("move_left", "move_right", "move_forward", "move_backward")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)

	move_and_slide()

func _update_camera(delta):
	_mouse_rotation.x += _tilt_input * delta
	_mouse_rotation.x = clamp(_mouse_rotation.x, _TILT_LOWER_LIMIT, _TILT_UPPER_LIMIT)
	_mouse_rotation.y += _rotation_input * delta
	
	_player_rotation = Vector3(0.0, _mouse_rotation.y, 0.0)
	_camera_rotation = Vector3(_mouse_rotation.x, 0.0, 0.0)
	
	_camera.transform.basis = Basis.from_euler(_camera_rotation)
	_camera.rotation.z = 0.0
	
	global_transform.basis = Basis.from_euler(_player_rotation)
	
	_rotation_input = 0.0
	_tilt_input = 0.0

func turn_on_flashlight():
	_flashlight.show()

func turn_off_flashlight():
	_flashlight.hide()
