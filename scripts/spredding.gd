#@tool
extends CharacterBody3D

signal found_player

@export var speed: float = 2
@export var acceleration = 5
@export var target: CharacterBody3D

@onready var _nav_agent: NavigationAgent3D = find_child("NavigationAgent3D")

func _physics_process(delta):
	if speed == 0:
		return
	_nav_agent.target_position = target.global_position
	var direction: Vector3 = _nav_agent.get_next_path_position() - global_position
	var look_direction = target.global_position
	look_direction.y = global_position.y
	look_at(look_direction)
	direction = direction.normalized()
	velocity = velocity.lerp(direction * speed, acceleration * delta)
	move_and_slide()

func _on_navigation_agent_3d_target_reached():
	print("Spredding Wins ", global_position.distance_to(target.global_position))
	found_player.emit()
