extends CanvasLayer

func _on_start_button_pressed():
	Global.start_game()

func _on_quit_button_pressed():
	Global.quit_game()
